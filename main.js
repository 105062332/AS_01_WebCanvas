
var _canvas = document.getElementById('canvas');
var ctx = _canvas.getContext('2d');
var r = document.getElementById("red");
var g = document.getElementById("green");
var b = document.getElementById("blue");
var vr = document.getElementById("vred");
var vg = document.getElementById("vgreen");
var vb = document.getElementById("vblue");
var casual = document.getElementById("casual");
var undo = document.getElementById("Undo");
var redo = document.getElementById("Redo");
var eraser=document.getElementById("eraser");
var brush = document.getElementById("size");
var vbrush = document.getElementById("vsize");
var text=document.getElementById("text");
var font=document.getElementById("font");
var reset=document.getElementById("Reset");
var rec=document.getElementById("rec");
var down = document.getElementById("down");
var cir = document.getElementById("circle");
var tri = document.getElementById("trian");
var drawl=0;
var triang = 0;
var cas=1;
var erase=0;
var circle=0;
var cPushArray = new Array();
var cStep = -1;
var element = null;
var rectangular=0;
var word=0;
var str;

cPush();


///////// Selector ///////////
ctx.strokeStyle = "rgb("+r.value+","+g.value+","+b.value+")";
ctx.lineWidth=brush.value;
vr.value = r.value;
vb.value = b.value;
vg.value = g.value;
vbrush.value=brush.value;
function changeRed(value)
{
  r.value = value;
  ctx.strokeStyle = "rgb("+r.value+","+g.value+","+b.value+")";
  vr.value = r.value;
}
function changeGreen(value)
{
  g.value = value;
  ctx.strokeStyle = "rgb("+r.value+","+g.value+","+b.value+")";
  vg.value = g.value;
}
function changeBlue(value)
{
  b.value = value;
  ctx.strokeStyle = "rgb("+r.value+","+g.value+","+b.value+")";
  vb.value = b.value;
}
function changeSize(value)
{
  brush.value=value;
  ctx.lineWidth=value;
  vbrush.value=value;
}

var x = 0;
var y = 0;

var mouse = {
  startX: 0,
  startY: 0
};

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };   
};

function mouseMove(evt) {
  var mousePos = getMousePos(_canvas, evt);
  if(rectangular==1){
      if(drawl==1){
        ctx.clearRect(0,0,600,400);
        var canvasPic = new Image();
        canvasPic.onload = function () {ctx.drawImage(canvasPic, 0, 0); };
        canvasPic.src = cPushArray[cStep-1];
        ctx.beginPath();
        ctx.moveTo(mouse.startX,mouse.startY);
        ctx.lineTo(mousePos.x,mousePos.y);
        ctx.stroke();
      }else{
        ctx.fillStyle = "rgb("+r.value+","+g.value+","+b.value+")";
        ctx.fillRect(mouse.startX,mouse.startY, mousePos.x-mouse.startX, mousePos.y-mouse.startY);
      }
  }
  else if(circle==1){
    var radius = Math.sqrt(Math.pow(mousePos.x-mouse.startX,2)+ Math.pow(mousePos.y-mouse.startY,2));
    ctx.fillStyle = "rgb("+r.value+","+g.value+","+b.value+")";
    ctx.beginPath();
    ctx.arc(mouse.startX,mouse.startY,radius,0,2*Math.PI);
    ctx.fill();
  }
  else if(cas==1)
  {
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
  }
};


_canvas.addEventListener('mousedown', function(evt) {
  var mousePos = getMousePos(_canvas, evt);
  //從按下去就會執行第一次的座標取得
  //evt.preventDefault();
  if(drawl==0 && cas==1 && rectangular==0 && word==0 && circle==0)
  {
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);   
    _canvas.addEventListener('mousemove', mouseMove, false);
  }
  else if(rectangular==1)
  {
      mouse.startX = mousePos.x;
      mouse.startY = mousePos.y;
      console.log(mouse.startX,mouse.startY);
      element = document.createElement('div');
      /*element.style.color = 'red';
      element.style.left = mousePos.x + 'px';
      element.style.top = mousePos.y + 'px';
      _canvas.appendChild(element);*/
      _canvas.style.cursor = "crosshair";
      _canvas.addEventListener('mousemove',mouseMove,false);
  }
  else if(circle==1)
  {
    mouse.startX = mousePos.x;
    mouse.startY = mousePos.y;
    _canvas.style.cursor = "crosshair";
    _canvas.addEventListener('mousemove',mouseMove,false);
  }
  else if(word==1)
  {
    ctx.font = brush.value + "px " +font.value;
    console.log(ctx.font);
    ctx.fillText(str,mousePos.x,mousePos.y);
    _canvas.addEventListener('mousemove',mouseMove,false);
  }
  else if(triang==1)
  {
    ctx.fillStyle = "rgb("+r.value+","+g.value+","+b.value+")";
    ctx.beginPath();
    ctx.moveTo(mousePos.x,mousePos.y);
    ctx.lineTo(mousePos.x+25,mousePos.y+25);
    ctx.lineTo(mousePos.x+25,mousePos.y-25);
    ctx.fill();
  }
});

_canvas.addEventListener('mouseup', function() {
    _canvas.style.cursor = "default";
    _canvas.removeEventListener('mousemove', mouseMove, false);
    cPush();
}, false);

rec.addEventListener('click',function(){
  ctx.strokeStyle = "rgb("+r.value+","+g.value+","+b.value+")";
  _canvas.style.cursor="crosshair";
  cas=0;
  rectangular=1;
  erase=0;
  word=0;
  circle = 0;
  triang = 0;
})

casual.addEventListener('click',function(){
  ctx.strokeStyle = "rgb("+r.value+","+g.value+","+b.value+")";
  _canvas.style.cursor="default";
  cas=1;
  rectangular=0;
  erase=0;
  word=0;
  circle = 0;
  triang = 0;
})

cir.addEventListener('click',function(){
  _canvas.style.cursor="crosshair";
  cas=0;
  rectangular=0;
  erase=0;
  word=0;
  circle = 1;
  triang = 0;
})

tri.addEventListener('click',function(){
  _canvas.style.cursor="point";
  cas=0;
  rectangular=0;
  erase=0;
  word=0;
  circle = 0;
  triang = 1;
})

////Undo Redo///
function cPush() {
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(_canvas.toDataURL());
}

undo.addEventListener("click",cUndo);
redo.addEventListener("click",cRedo);

function cUndo() {
  if (cStep > 0) {
      cStep--;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      ctx.clearRect(0,0,600,400); 
      canvasPic.onload = function () {ctx.drawImage(canvasPic, 0, 0); }
  }
}

function cRedo() {
  if (cStep < cPushArray.length-1) {
      cStep++;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      ctx.clearRect(0,0,600,400);
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
  }
}

//// eraser ////
eraser.addEventListener("click",function(){
  ctx.strokeStyle = "white";
  drawl=0;
  cas=1;
  rectangular=0;
  erase=1;
  word=0;
  _canvas.style.cursor="cell";
  circle = 0;
  triang = 0;
})

//// word ////
text.addEventListener("click",function(){
  str = prompt("Please enter the word, and click on canvas","");
  word=1;
  drawl=0;
  cas=0;
  rectangular=0;
  erase=0;
  _canvas.style.cursor="text";
  circle = 0;
  triang = 0;

})

//// Reset ////
reset.addEventListener("click",function(){
  _canvas.style.cursor="default";
  cPush();
  ctx.clearRect(0,0,600,400);
})
