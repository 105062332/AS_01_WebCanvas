# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
* HTML
    在HTML內主要分成三個部分，一是畫布"canvas"的部分，二是調整各參數的"input"，
    最後是按鈕的部分，用來切換模式。
* CSS
    調整按鈕形狀，並使其自動符合內容大小。
* JavaScript
    JS的程式碼大致上也可以分成幾部分:
    * Selector:
        我R、G、B分別建立一個Input取值(0~255)，而當其value改變時，會個別觸發changeRed、
        changeBlue、changeGreen，修改畫筆的顏色。
        筆刷大小也是以相同的方式進行調整。
    * Mode:
        一個按鈕對應一個變數，click on button時對應的變數變成1，其餘的變數設定為0，利用這
        些變數判斷屬於哪個模式，然此方法需要建立大量變數，而每次修改、新增功能時幾乎需要修
        改整份code的程式碼，若以修改單一string或單一變數配合switch語法，除了免去許多判斷
        式以外，也更容易debug。
    * Draw:
        按照筆刷畫出對應的圖案，於casual模式下按住並拖曳滑鼠可以劃出一條連續的線，另外正方形
        與圓形也是按住拖曳，來形成一個完整的圖形，而放開滑鼠以後會將目前的畫布儲存，以便進行
        Undo和Redo功能。 
        eraser將白色覆蓋上去，點擊Text後會出現一個對話框，在輸入指定字串後點擊畫布，則會將
        輸入的字串顯示在畫布上，畫布右邊有調整字體大小與字型的選單。
        點擊下載後利用超連結的download attribute將canvas下載下來，檔名為"text.png"。
        另外以clearRect()的function實作reset。